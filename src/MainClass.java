import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MainClass {

	public static final int TAB_SIZE=25;

	public static void main(String[] args) {
		int[][] A=new int[TAB_SIZE][TAB_SIZE]; 
		int[][] B=new int[TAB_SIZE][TAB_SIZE];
		int[][] C=new int[TAB_SIZE][TAB_SIZE];
		for(int i=0;i<TAB_SIZE;i++){
			for(int j=0;j<TAB_SIZE;j++){
				A[i][j]=1;
				B[i][j]=1;
				C[i][j]=0;
			}
		}
		System.out.println("Macierze przed mno�eniem:");
		System.out.println("A="+Arrays.deepToString(A));
		System.out.println("B="+Arrays.deepToString(B));
		System.out.println("C="+Arrays.deepToString(C));
		Thread[][] t=new Thread[TAB_SIZE][TAB_SIZE];
		long startTime=System.currentTimeMillis();

		ExecutorService pool=Executors.newFixedThreadPool(10);

		for(int i=0;i<TAB_SIZE;i++){
			for(int j=0;j<TAB_SIZE;j++){
				pool.execute(new MatrixElementTask(i, j, A, B, C));
			}
		}
		pool.shutdown();
		try {
			if(pool.awaitTermination(1, TimeUnit.SECONDS)){
				long stopTime=System.currentTimeMillis();
				System.out.println("Macierz C po mno�enu: ("+(stopTime-startTime)+" milisekund)");
				System.out.println("C="+Arrays.deepToString(C));
			}else{
				System.out.println("Limit czasu up�yn�� przed zako�czeniem pacy wszystkich w�tk�w"); 
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}



}
